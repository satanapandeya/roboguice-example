package com.example.satan.roboguiceexample;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;

public class MainActivity extends RoboActivity {

    @InjectView(R.id.string_app_name) TextView  string_head_text;
    @InjectResource(R.string.app_name) String var_app_name;

    @InjectView(R.id.string_id) TextView string_roboDef_text;
    @InjectResource(R.string.string_says) String var_roboDef_text;

    @InjectView(R.id.button_Robo_btn) Button  var_robo_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        string_head_text.setText(var_app_name);
        string_roboDef_text.setText(var_roboDef_text);

        var_robo_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                var_robo_btn.setText("Got Clicked");
            }
        });

    }
}
